class CreateFiles < ActiveRecord::Migration
  def change
    create_table :files do |t|
      t.string :name
      t.string :user_friendly_name
      t.string :location

      t.timestamps
    end
  end
end
