class CreateUserFiles < ActiveRecord::Migration
  def change
    create_table :user_files do |t|
      t.string :name
      t.string :friendly_name
      t.string :location

      t.timestamps
    end
  end
end
