class AddUserToUserFiles < ActiveRecord::Migration
  def change
    add_reference :user_files, :user, index: true
  end
end
