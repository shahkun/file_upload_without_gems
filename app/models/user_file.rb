class UserFile < ActiveRecord::Base
  belongs_to :user

  before_destroy :remove_file_from_disk

  private

  def remove_file_from_disk
    puts "ok removing file called"
    File.delete(self.location)
  end
end
