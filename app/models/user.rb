class User < ActiveRecord::Base
  has_secure_password

  validates_uniqueness_of :email
  validates :password, :length => { :minimum => 5 }
  validates_confirmation_of :password
  after_create :create_user_directory

  has_many :user_files

  def get_user_dir
    "#{Rails.root}/storage/#{self.id}"
  end

  private

  def create_user_directory
    Dir.mkdir(get_user_dir)
  end
end
