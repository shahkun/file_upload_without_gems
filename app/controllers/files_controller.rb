class FilesController < ApplicationController
  before_filter :authorize, only: [:upload, :delete]

  def index
    @user = current_user
    @files = @user.user_files if !@user.nil?
  end

  def upload
    uploaded_io = params[:upload]
    user = current_user
    user_dir = user.get_user_dir
    file_path = write_file_to_disk(user_dir, uploaded_io)
    user.user_files.create(:name => "#{file_path.split('/').last}", :location => "#{file_path}")
    redirect_to root_url
  end

  def destroy
    @my_file = UserFile.find(params[:id])
    # Only the user who created the file can destroy it.
    if @my_file.user_id == current_user.id
      @my_file.destroy
      redirect_to root_url
    else
      flash.now.alert = "Error! You are not authorized to delete this file!"
      redirect_to root_url, flash: {error: "Not authorized!"}
    end
  end

  private

  def write_file_to_disk(dir, file_io)
    full_file_path = "#{dir}/#{file_io.original_filename}"
    File.open(full_file_path, 'wb') do |file|
      file.write(file_io.read)
    end
    full_file_path
  end

end
